#include <vector>
#include <cstdlib>
#include <pcap.h>
#include "mac.h"

#define ESSID_LEN 32
#define ETHER_ADDR_LEN 6

struct ieee80211_radiotap_header {
        u_int8_t        it_version;     /* set to 0 */
        u_int8_t        it_pad;
        u_int16_t       it_len;         /* entire length */
        u_int32_t       it_present;     /* fields present */
} __attribute__((__packed__));
typedef ieee80211_radiotap_header rthdr;

struct ieee80211_mac_header
{
	u_int16_t frame_control;
	u_int16_t id;
	Mac dmac;
	Mac smac;
	Mac bss;
	u_int16_t seq;
};
typedef ieee80211_mac_header machdr;

struct Wireless
{
    u_int8_t not_use[13];
    u_int8_t ssid_len;
}__attribute__((__packed__));
typedef Wireless wireless;

struct vectorer
{
	Mac bssid;
	int beacons;
	char essid[ESSID_LEN];
};
using namespace std;

vector<vectorer> list;

void printxxd(u_char* arr, int n)
{
	for(int i = 0; i < n; i++)
	{
		printf("%02x ", arr[i]);	
	}
}



void printMac(u_int8_t* mac_arr)
{
	for(int i = 0; i < ETHER_ADDR_LEN - 1; i++)
	{
		printf("%02x:", mac_arr[i]);
	}
	printf("%02x", mac_arr[ETHER_ADDR_LEN - 1]);
}

void usage()
{
	printf("syntax : sudo ./airodump <interface>\n");
	printf("sample : sudo ./airodump mon0\n");
}

void printinfo()
{
	system("clear");
	printf("BSS\t\t\tbeacons\tESSID\n");
	for(int i = 0; i < list.size(); i++)
    {
    	printMac((u_int8_t*)list[i].bssid);
		printf("\t%d\t%s\n", list[i].beacons, list[i].essid);
    }
}

int main(int argc, char* argv[]) {
	if(argc != 2)
	{
		usage();
		return 0;
	}
	char *dev = argv[1];
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t* pcap = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
	if (pcap == NULL) {
		fprintf(stderr, "pcap_open_live(%s) return null - %s\n", dev, errbuf);
		return -1;
	}

	struct pcap_pkthdr* header;
	const u_char* packet = (u_char*)malloc(BUFSIZ);
	if(packet == NULL)
	{
		fprintf(stderr, "malloc return null\n");
		return -1;
	}

	while (true) {
		int res = pcap_next_ex(pcap, &header, &packet);
		if (res == 0) continue;
		if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK) {
			printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(pcap));
			break;
		}
		rthdr* rtap = (rthdr*)packet;
    	machdr* beacon = (machdr*)(packet + rtap->it_len);
    	wireless* wless = (wireless*)(packet + rtap->it_len + sizeof(machdr));
    	char* ssid = (char*)(packet + rtap->it_len + sizeof(machdr) + sizeof(wireless));
    	if(beacon->frame_control != 0x80)
    		continue;
    	
    	int is_present = 0;
    	for(int i = 0; i < list.size(); i++)
    	{
    		if(list[i].bssid == beacon -> bss)
    		{
    			list[i].beacons++;
    			is_present = 1;
    		}
    	}
    	if(is_present == 1)
    	{
    		printinfo();
    		continue;
    	}

    	struct vectorer new_info;
    	new_info.bssid = beacon -> bss;
    	new_info.beacons = 1;
    	//printf("here\n");
    	//printxxd((u_char*)ssid - 20, 40);
    	memset(new_info.essid, 0, ESSID_LEN);
    	memcpy(new_info.essid, ssid, wless -> ssid_len);
    	list.push_back(new_info);
    	printinfo();
	}
	free((u_char*)packet);
	pcap_close(pcap);
	return 0;
}

